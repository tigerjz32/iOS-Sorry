//
//  ViewController.swift
//  Sorry! Deck
//
//  Created by Jay Kansagra on 6/14/16.
//  Copyright © 2016 TreeTiger. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var pressedButton = ""
    
    @IBAction func ClassicModePressed(sender: AnyObject) {
        pressedButton = "ClassicMode"
    }

    @IBAction func SpicyModePressed(sender: AnyObject) {
        pressedButton = "SpicyMode"
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ButtonPressed" {
            if let DeckViewController = segue.destinationViewController as? DeckViewController {
                DeckViewController.pressedButton = pressedButton
            }
        }
    }
    
}

