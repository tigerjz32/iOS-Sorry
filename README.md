# iOS-Sorry
Sorry! board game card deck for iOS devices. There are two types of decks in this app, 'Classic' and'Spicy'. 'Classic' allows users to play with the classic set of deck that they would have gotten with the board game. 'Spicy' deck allows users to play the game with randomly generated deck of cards.
