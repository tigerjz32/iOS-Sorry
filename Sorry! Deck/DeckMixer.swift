//
//  DeckMixer.swift
//  Sorry! Deck
//
//  Created by Jay Kansagra on 6/23/16.
//  Copyright © 2016 TreeTiger. All rights reserved.
//

import UIKit

class DeckMixer {
    
//    var decks = [[String]]()
//    
//    init (decks: [[String]]) {
//        for d: [String] in decks {
//            self.decks.append(d)
//        }
//    }
//    
//    func mix()->[String] {
//        
//        for x in 0 ..< decks.count {
//            for y in 0 ..< decks[x].count {
//                ("\(x)  \(y)")
//            }
//        }
//        
//        return decks[1]
//    }
    
    var mixedDeck = [String]()
    
    init (deck1: [String], deck2: [String], deck3: [String]) {
        
        for d1: String in deck1 {
            for d2: String in deck2 {
                for d3: String in deck3 {
                    if d1 == "Sorry! " {
                        self.mixedDeck.append(d1 + "" + "")
                    } else if d1 == "Move 1 " && d3 == "or split" {
                        self.mixedDeck.append(d1 + d2 + "")
                    } else {
                        self.mixedDeck.append(d1 + d2 + d3)
                    }
                }
            }
        }
        
    }
    
    func getMixedDeck()->[String] {
        return mixedDeck
    }
    
    func getMixedDeckCount()->Int {
        return mixedDeck.count
    }
    
}