//
//  Deck.swift
//
//
//  Created by Jay Kansagra on 6/18/16.
//
//

import UIKit

class Deck {
    
    var players = [UIColor]()
    var cards = [String]()
    var tips = [String]()
    
    var player = -1
    var card = -1
    
    init () {
        
    }
    
    init (players: [UIColor], cards: [String], tips: [String]) {
        
        for p: UIColor in players {
            self.players.append(p)
        }
        for c: String in cards {
            self.cards.append(c)
        }
        for t: String in tips {
            self.tips.append(t)
        }
        
    }
    
    func getPlayer()->UIColor {
        return players[player]
    }
    
    func getTip()->String {
        return tips[card]
    }
    
    func getCard()->String {
        return cards[card]
    }
    
    func setPlayer() {
        if player == -1 {
            player = Int(arc4random_uniform(UInt32(players.count)))
        } else if player == 3 {
            player = 0;
        } else {
            player += 1
        }
    }
    func setCard() {
       card = Int(arc4random_uniform(UInt32(cards.count)))
    }
    
}