//
//  ClassicViewController.swift
//  Sorry! Deck
//
//  Created by Jay Kansagra on 6/19/16.
//  Copyright © 2016 TreeTiger. All rights reserved.
//

import UIKit

class DeckViewController: UIViewController {
    
    @IBOutlet var MainView: UIView!
    @IBOutlet var Result: UILabel!
    @IBOutlet var InfoButton: UIButton!

    var pressedButton = ""
    private var deck = Deck()
    private var players = [UIColor(red: 1.0, green: 0.3, blue: 0.3, alpha: 1.0), UIColor(red: 0.3, green: 0.75, blue: 0.1, alpha: 1.0), UIColor(red: 0.95, green: 0.9, blue: 0.05, alpha: 1.0), UIColor(red: 0.2, green: 0.5, blue: 1.0, alpha: 1.0)]
    private var spicyDeck1 = ["Sorry! ", "Move 1 ", "Move 2 ", "Move 3 ", "Move 4 ", "Move 5 ", "Move 6 ", "Move 7 ", "Move 8 ", "Move 9 ", "Move 10 ","Move 11 ", "Move 12 "];
    private var spicyDeck2 = ["", "forward ", "forward ", "forward ", "backward "];
    private var spicyDeck3 = ["or split", "or split", "or start", "or start", "or switch", "or switch", "and take another turn", "", "", "", "", "", ""];
    private var classicCards = ["Move 1 forward OR start", "Move 2 forward OR start and take another turn", "Move 3 forward", "Move 4 backward", "Move 5 forward", "Move 7 forward OR split", "Move 8 forward", "Move 10 backward", "Move 11 forward OR switch", "Move 12 forward", "Sorry!"];
    private var classicTips = ["Move a pawn from Start or move a pawn one space forward.", "Move a pawn from Start or move a pawn two spaces forward. Drawing a two entitles the player to draw again at the end of his or her turn. If you cannot use two, you can still draw again.", "Move a pawn three spaces forward.", "Move a pawn four spaces backward.", "Move a pawn five spaces forward.", "Move one pawn seven spaces forward or split the seven spaces between two pawns (such as four spaces for one pawn and three for another). This makes it possible for two pawns to enter Home on the same turn, for example. The seven cannot be split into a six and one or a five and two for the purposes of moving out of Start. The entire seven spaces must be used one way or the other or the turn is lost. You also may not move backwards with a split.", "Move a pawn eight spaces forward.", "Move a pawn 10 spaces forward or one space backward. If a player cannot go forward 10 spaces, then one pawn must go back one space.", "Move 11 spaces forward or switch places with one opposing pawn. A player that cannot move 11 spaces is not forced to switch and instead can forfeit the turn.", "Move a pawn 12 spaces forward.", "Move any one pawn from Start to a square occupied by any opponent, sending that pawn back to its own Start. If there are no pawns on the player's Start, or no opponent's pawns on any squares, the turn is lost. If an enemy's pawn is swapped while it is in front of your HOME, your pawn is switched EXACTLY where your enemy's pawn is, not at your HOME."];

    @IBAction func ClassicInfo(sender: AnyObject) {
        let alertController = UIAlertController(title: "Rule", message:
            deck.getTip(), preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))

        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func Tap(sender: AnyObject) {
        deck.setCard()
        deck.setPlayer()
        Result.text = deck.getCard()
        self.MainView.backgroundColor = deck.getPlayer();
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.darkGrayColor()
        InfoButton.hidden = true
        
        if pressedButton == "ClassicMode" {
            self.title = "Classic"
            deck = Deck(players: players, cards: classicCards, tips: classicTips)
            InfoButton.hidden = false
        } else if pressedButton == "SpicyMode" {
            self.title = "Spicy"
            let deckMixer = DeckMixer(deck1: spicyDeck1, deck2: spicyDeck2, deck3: spicyDeck3)
            deck = Deck(players: players, cards: deckMixer.getMixedDeck(), tips: [String](count: deckMixer.getMixedDeckCount(), repeatedValue: ""))
        } else {
            self.title = nil
        }
    }
    
}
