//
//  RulebookViewController.swift
//  Sorry! Deck
//
//  Created by Jay Kansagra on 6/26/16.
//  Copyright © 2016 TreeTiger. All rights reserved.
//

import UIKit
import Foundation

class RulebookViewController: UIViewController {
    
    @IBOutlet var webViewInstance: UIWebView!
    var urlAddress = "https://en.wikipedia.org/wiki/Sorry!_(game)#Classic_cards_and_function"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIWebView.loadRequest(webViewInstance)(NSURLRequest(URL: NSURL(string: urlAddress)!))
    }
    
}

